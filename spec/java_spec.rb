require 'serverspec'

set :backend, :exec


describe command('java -version') do
    its(:stderr) { should include 'java version "1.8' }
    its(:exit_status) { should eq 0 }
end

describe file('/usr/java') do
    it { should be_directory }
end